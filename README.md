# Project Potera :memo:
**Web adaptacija popularnog kviza Potera.**

## Table of contents :book:
* [General info](#general-info-point_down)
* [Technologies](#technologies-wrench)
* [Setup](#setup-dragon_face)
* [Playing instructions](#playing-instructions-smile)
* [Video](#video-video_camera)
* [Developers](#developers-computer)

## General info :point_down:
**Cilj je sakupiti što više novca u prvoj fazi igre zatim odgovoriti na dovoljno pitanja u drugoj fazi
pre nego što tragač stigne do vas.**

## Technologies :wrench:
- **Node.js**
- **Express.js**
- **MongoDB SUBP. Mongoose.js**
- **Angular**

## Setup :dragon_face:
Klonirati repozitorijum i pozicionirati se u njega:
```

$ git clone https://gitlab.com/matfpveb/projekti/2020-2021/09-Potera.git
$ cd 09-Potera

Pokretanje:
$ cd src/server
$ npm install
$ cd ../src/client
$ npm install
$ cd ../..
$ mongorestore -d PoteraBase ./Dbexport

Zatim otvoriti 2 terminala:
    U Prvom:
        $ cd src/server
        $ npm start
    U drugom:
        $ cd src/client
        $ ng serve
```

## Playing instructions :smile:
**1.Prva faza**
Pitanja se izvlače, potrebno je odgovoriti na tačan odgovor klikom na njega.
Prva faza traje 60 sekundi.

**2.Druga faza**
Kliknuti pokreni igru, odgovoriti na pitanje pa kliknuti sledeće pitanje kako bi se
sledeće pitanje postavilo.

## Video :video_camera:
* [Potera](https://gitlab.com/matfpveb/projekti/2020-2021/09-Potera/-/wikis/Snimak-Rada-Aplikacije)

## Developers :computer:

- [Nemanja Lisinac, 478/2017](https://gitlab.com/lisk0)
- [Slobodan Jovanovic, 186/2017](https://gitlab.com/Sloba98)
- [Luka Miletic, 91/2017](https://gitlab.com/lukamileticc)
- [Mihailo Trisovic, 474/2018](https://gitlab.com/mihailotrisovic)
