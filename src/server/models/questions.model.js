const mongoose = require ('mongoose')

const questionSchema = mongoose.Schema({
    tekst: {type: String, unique: true, required: true, index:true},
    odgovor1: String,
    odgovor2: String,
    odgovor3: String,
    tacanodgovor: Number
});

const questionModel = mongoose.model('Questions', questionSchema);

module.exports= questionModel;