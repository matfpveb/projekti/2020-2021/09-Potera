const mongoose = require ('mongoose')
var crypto = require('crypto');
const userSchema = mongoose.Schema({
    ime: String,
    prezime: String,
    email: {type: String, unique: true, required: true, index:true},
    rodjendan: Date,
    hash : String,
    salt : String
});

userSchema.methods.setPassword = function(password) {
     
    // Creating a unique salt for a particular user
       this.salt = crypto.randomBytes(16).toString('hex');
     
       // Hashing user's salt and password with 100 iterations,
       //64 length and sha512 digest
       this.hash = crypto.pbkdf2Sync(password, this.salt, 
       100, 64, 'sha512').toString('hex');
   };
     

   // Method to check the entered password is correct or not
   // valid password method checks whether the user
   // password is correct or not
   // It takes the user password from the request 
   // and salt from user database entry
   // It then hashes user password and salt
   // then checks if this generated hash is equal
   // to user's hash in the database or not
   // If the user's hash is equal to generated hash 
   // then the password is correct otherwise not
userSchema.methods.validPassword = function(password) {
       var hash = crypto.pbkdf2Sync(password, 
       this.salt, 100, 64, 'sha512').toString('hex');
       return this.hash === hash;
   };

const userModel = mongoose.model('Users', userSchema);

module.exports = userModel;