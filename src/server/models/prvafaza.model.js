const mongoose = require ('mongoose')

const prvafazaSchema = mongoose.Schema({
    tekst: {type: String, unique: true, required: true, index:true},
    odgovor1: String,
    odgovor2: String,
    odgovor3: String,
    tacanodgovor: Number
});

const prvafazaModel = mongoose.model('prvafaza', prvafazaSchema);

module.exports= prvafazaModel;