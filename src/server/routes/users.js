var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

const userModel = require ("../models/users.model");

/* GET ALL users */
router.get('/list', function(req, res) {
  
  userModel.find(function(err, userListResponse){
    if (err){
      res.send({status: 500, message: 'Unable to get all users'})
    }

    else {
      const num = userListResponse.length;
      res.send({status: 200, userNum: num, results: userListResponse});
    }
  });
});

/* Create new user */
router.post('/add', function(req, res) {
  /*let userObj = new userModel({
    ime:'Petar', 
    prezime:'Peric' ,
    email: 'peraperic2@gmail.com',
    rodjendan:'1998-06-11',
    hash: '',
    salt: ''
  }); */
  let userObj= new userModel();
  
  userObj.ime = req.body.ime
  userObj.prezime = req.body.prezime
  userObj.email = req.body.email
  userObj.rodjendan= req.body.rodjendan
  userObj.setPassword(req.body.sifra);

  userObj.save(function(err, userObj){
    if (err){
      res.send({status: 500, message: 'Unable to add user'})
    }

    else {
      res.send({status: 200, message: 'user added', userDetails: userObj});
    }

  });
});

/* Update user */
router.put('/update', function(req, res) {
  
  const userId = req.query.userId;

  let userObj = new userModel();
  userObj.ime= req.body.ime;
  userObj.prezime= req.body.prezime;
  userObj.email= req.body.email;
  userObj.rodjendan= req.body.rodjendan;
  if (req.body.sifra==null){
    userObj.hash=req.body.hash;
    userObj.salt=req.body.salt;
  }
  else {userObj.setPassword(req.body.sifra);}
  userObj._id= userId;

  userModel.findByIdAndUpdate (userId, userObj,function(err, userResponse){
    if (err){
      res.send({status: 500, message: userResponse})
    }

    else {
      res.send({status: 200, userResponse});
    }

    });

});


/* Delete user*/
router.delete('/delete', function(req, res) {
  const userId = req.query.userId;

  userModel.findByIdAndDelete (userId, function(err, userResponse){
    if (err){
      res.send({status: 500, message: 'error'})
    }

    else {if(userResponse!= null)
      res.send({status: 200, message: 'Deleted successfully', objekat: userResponse});

      else res.send({status: 500, message: 'User not found'})
    }

    });

});


/* Search user */
router.get('/search', function(req, res, next) {
  
  
  userModel.findOne ({ email : req.body.email }, function(err, userResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find user'})
    }

    else {
      res.send({status: 200, userResponse});
    }

    });

});

router.post('/login', (req, res) => {
  

  userModel.findOne({ email : req.body.email }, function(err, userResponse) {
      if (userResponse === null) {
          res.send({status: 500, message: 'Unable to find user'});
      }
      else {
          if (userResponse.validPassword(req.body.password)) {
              res.send({status: 201, message: 'User logged in successfully'});
          }
          else {
              res.send({status: 500, message: 'Wrong password'});
          }
      }
  });
});

router.get('/id', function(req, res, next) {
  const userId = req.query.userId;
  
  userModel.findById (userId, function(err, userResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find user'})
    }

    else {
      res.send({status: 200, userResponse});
    }

    });

});

module.exports = router;
