var express = require('express');
const { Query } = require('mongoose');
var router = express.Router();

var mongoose = require('mongoose');

const questionModel = require ("../models/questions.model");

/* GET ALL questions */
router.get('/list', function(req, res, next) {
  
  questionModel.find(function(err, questionListResponse){
    if (err){
      res.send({status: 500, message: 'Unable to get all questions'})
    }

    else {
      const num = questionListResponse.length;
      res.send({status: 200, brPitanja: num, results: questionListResponse});
    }
  });
});


/* Create new question */
router.post('/add', function(req, res, next) {
  let questionObj = new questionModel();
  
  questionObj.tekst= req.body.tekst;
  questionObj.odgovor1= req.body.odgovor1;
  questionObj.odgovor2= req.body.odgovor2;
  questionObj.odgovor3= req.body.odgovor3;
  questionObj.tacanodgovor= req.body.tacanodgovor;

  questionObj.save(function(err, questionObj){
    if (err){
      res.send({status: 500, message: 'Unable to add question'})
    }

    else {
      res.send({status: 200, message: 'Question added', questionDetails: questionObj});
    }

  });
});

/* Update question */
router.put('/update', function(req, res, next) {
  const questionId = req.query.questionId;
  
  /*let questionObj = {
    id: 1,
    tekst: 'Koja je najnaseljenija zemlja na svetu?',
    odgovor1: 'Rusija',
    odgovor2: 'Kina',
    odgovor3: 'Indija',
    tacanodgovor: 2
  };
  */
  
  let questionObj = new questionModel();
  
  questionObj.tekst= req.body.tekst;
  questionObj.odgovor1= req.body.odgovor1;
  questionObj.odgovor2= req.body.odgovor2;
  questionObj.odgovor3= req.body.odgovor3;
  questionObj.tacanodgovor= req.body.tacanodgovor;
  questionObj._id= questionId;

  questionModel.findByIdAndUpdate( questionId, questionObj,function(err, questionResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find question'})
    }

    else {
      res.send({status: 200, questionResponse});
    }

    });

});


/* Delete question */
router.delete('/delete', function(req, res, next) {
  const questionId = req.query.questionId;
  
  questionModel.findByIdAndDelete (questionId, function(err, questionResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find question'})
    }

    else {
      res.send({status: 200, message: 'Deleted successfully'});
    }

    });

});


/* Search question */
router.get('/search', function(req, res, next) {
  
  questionModel.findOne ({ tekst : req.body.tekst }, function(err, questionResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find question'})
    }

    else {
      res.send({status: 200, questionResponse});
    }

    });

});

/* Search question */
router.get('/id', function(req, res, next) {
  const questionId = req.query.questionId;

  questionModel.findById (questionId, function(err, questionResponse){
    if (err){
      res.send({status: 500, message: 'Unable to find question'})
    }

    else {
      res.send({status: 200, questionResponse});
    }

    });

});

module.exports = router;
