import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [{ path: 'adminpage', loadChildren: () => import('./adminpage/adminpage.module').then(m => m.AdminpageModule) },

{ path: 'TimeChase', loadChildren: () => import('./time-chase/time-chase.module').then(m => m.TimeChaseModule) },

{ path: 'Chase', loadChildren: () => import('./chase/chase.module').then(m => m.ChaseModule) },

{ path: '', redirectTo: 'adminpage/faza1', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
