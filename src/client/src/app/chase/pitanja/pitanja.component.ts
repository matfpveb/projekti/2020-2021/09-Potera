import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.dev'
import {Pitanje} from './pitanje'
import { ChasePhasesCommonService } from 'src/app/services/chase-phases-common.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pitanja',
  templateUrl: './pitanja.component.html',
  styleUrls: ['./pitanja.component.css']
})
export class PitanjaComponent implements OnInit {
  //kad se pritisne dugme zapocni igru ovo postaje true
  gameIsRunning:boolean;

  private wrongAnswerBuzzer = new Audio();
  private correctAnswerBuzzer = new Audio();

  // u pitanje.ts je definisana klasa Pitanje
  pitanja: Pitanje [] = [];

  private RandomQuestionNumberArray: number [];

  numberOfQuestion: number = 0;

  questionText: string = "";
  answer_1: string = "";
  answer_2: string = "";
  answer_3: string = "";
  correctAnswer: number = 0;


  buttonPressed: boolean = true;


  // salje se korenom elementu
  @Output()
  sendAnswerCounter = new EventEmitter;


  correctAnswerCounter: number = 0;

  @Output()
  sendNumOfChassersCorrect = new EventEmitter;
  numberOfChassersCorrect:number = 0;

  ROW_DIFFERENCE_CHASER_PLAYER:number = 3;


  constructor(private httpClient: HttpClient, private CPservice:ChasePhasesCommonService, private router:Router) {
    this.gameIsRunning = false;
    this.wrongAnswerBuzzer.src = '../../../assets/test.mp3';
    this.correctAnswerBuzzer.src = '../../../assets/Correct AnswerIdea Sound Effects.mp3';
   }

   lista: any;
   svapitanja: any;
   brpitanja: number;

   getQuestions(){
    let url= environment.QUESTIONS_BASE_URL+environment.QUESTIONS.GET_ALL;
    let obs= this.httpClient.get(url);
    obs.subscribe((data: any[]) => {
      this.lista=data;
      this.svapitanja=this.lista.results;
      this.brpitanja=this.svapitanja.length;
      this.svapitanja.forEach(element => {
        this.pitanja.push(new Pitanje(
          element.tekst,
          element.odgovor1,
          element.odgovor2,
          element.odgovor3,
          element.tacanodgovor
        ));
      });
    });
   }

   editQuestion(id){

   }

   deleteQuestion(id){

   }

   searchQuestion(tekst){

   }



  ngOnInit(): void {
    this.getQuestions();
    this.wrongAnswerBuzzer.load();
    this.correctAnswerBuzzer.load();
    //this.generateRandomQuestionNumberArray();
  }
  public generateRandomQuestionNumberArray(){
    this.RandomQuestionNumberArray = Array.from(Array(this.pitanja.length).keys());
    //pomesa elemente u nizu
    this.RandomQuestionNumberArray.sort(() => Math.random() - 0.5);
  }
  //prvo pitanje se postavlja
  public initializeQuestions(){
    this.generateRandomQuestionNumberArray();

    this.questionText = this.pitanja[this.RandomQuestionNumberArray[0]].text;
    this.answer_1 = this.pitanja[this.RandomQuestionNumberArray[0]].answer_1;
    this.answer_2 = this.pitanja[this.RandomQuestionNumberArray[0]].answer_2;
    this.answer_3 = this.pitanja[this.RandomQuestionNumberArray[0]].answer_3;
    this.correctAnswer = this.pitanja[this.RandomQuestionNumberArray[0]].correctAnswer;

  }
  public startGameButton(){
    this.initializeQuestions();
    this.gameIsRunning = true;
    this.answerTheQuestion();
  }

  public checkIfChasserWon(){
    if((this.numberOfChassersCorrect - this.correctAnswerCounter) === this.ROW_DIFFERENCE_CHASER_PLAYER){
      this.gameIsRunning = false;
      this.numberOfChassersCorrect = 0;
      this.correctAnswerCounter = 0;
      this.CPservice.setEndMessage('Tragac je pobedio!');
      this.CPservice.setEndOfGame();
      this.sendNumOfChassersCorrect.emit(this.numberOfChassersCorrect.toString());
    }
  }

 public checkIfPlayerWon(){
  if(this.correctAnswerCounter >= 5){
    this.CPservice.setEndMessage('Pobedili ste!');
    this.CPservice.setEndOfGame();
  }
 }

 public onButtonPressed(contestantsAnswer: number) {
    if(contestantsAnswer === this.correctAnswer) {
      this.correctAnswerBuzzer.play();
      this.correctAnswerCounter++;
      this.sendAnswerCounter.emit(this.correctAnswerCounter.toString());

      this.checkIfPlayerWon();
    }
    else {
      this.wrongAnswerBuzzer.play();
      this.checkIfChasserWon();
    }
    this.buttonPressed = false;

  }
  //dodeljuje se svako sledece pitanje osim prvog
  public nextQuestionButton() {
    this.buttonPressed = true;
    this.placeQuestionOnBoard();
  }

  public placeQuestionOnBoard() {
    this.numberOfQuestion++;

    this.questionText = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].text;
    this.answer_1 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_1;
    this.answer_2 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_2;
    this.answer_3 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_3;
    this.correctAnswer = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].correctAnswer;


    this.answerTheQuestion();
  }



  //Tragacev Deo
  public getRandomNumber() {
    var num = Math.random();

    if(num < 0.9){
      return this.correctAnswer;
    }

    if(num >= 0.9 && num < 0.95){
      if(this.correctAnswer != 2){
        return 2;
      }
      else {
        return 1;
      }
    }

    if(num >= 0.95 && num < 1){
      if(this.correctAnswer != 3){
        return 3;
      }
      return 1;
    }

    return 0;
  }

  public answerTheQuestion(){

    setTimeout(() =>
    {
      console.log('Tragac Razmislja');
    }, 3000);

    var answer = this.getRandomNumber();


    if(answer == this.correctAnswer){
      this.numberOfChassersCorrect++;
      this.sendNumOfChassersCorrect.emit(this.numberOfChassersCorrect.toString());
    }
    else {
    }
  }
}
