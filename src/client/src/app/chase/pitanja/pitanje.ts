export class Pitanje {
  constructor(
    public text: string,
    public answer_1: string,
    public answer_2: string,
    public answer_3: string,
    public correctAnswer: number
  ){}

}
