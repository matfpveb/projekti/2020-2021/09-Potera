import { ChasePhasesCommonService } from './../../services/chase-phases-common.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-end-pop-up',
  templateUrl: './end-pop-up.component.html',
  styleUrls: ['./end-pop-up.component.css']
})
export class EndPopUpComponent implements OnInit {

  constructor(public CPservice:ChasePhasesCommonService, private router:Router) {
   }

  ngOnInit(): void {
  }
}
