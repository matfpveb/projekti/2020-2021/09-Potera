import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndPopUpComponent } from './end-pop-up.component';

describe('EndPopUpComponent', () => {
  let component: EndPopUpComponent;
  let fixture: ComponentFixture<EndPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
