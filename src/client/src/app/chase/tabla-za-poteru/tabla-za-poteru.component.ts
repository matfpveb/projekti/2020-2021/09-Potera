import { ChasePhasesCommonService } from './../../services/chase-phases-common.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabla-za-poteru',
  templateUrl: './tabla-za-poteru.component.html',
  styleUrls: ['./tabla-za-poteru.component.css']
})
export class TablaZaPoteruComponent implements OnInit {

public valueOfRow:number;


/*
  U zavisnosti od ova dva broje se pomeraju igrac i tragac po tabli.
*/
@Input('numberOfCorrectAnswers')
public numberOfCorrectAnswers:number = 0;


@Input('numberOfChasersCorrectAnswers')
public numberOfChasersCorrectAnswers:number = 0;


  constructor(CPservice:ChasePhasesCommonService) {
    this.valueOfRow = CPservice.getMoneyEarned();
   }

  ngOnInit(): void {
  }

}
