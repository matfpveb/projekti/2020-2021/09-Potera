import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaZaPoteruComponent } from './tabla-za-poteru.component';

describe('TablaZaPoteruComponent', () => {
  let component: TablaZaPoteruComponent;
  let fixture: ComponentFixture<TablaZaPoteruComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaZaPoteruComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaZaPoteruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
