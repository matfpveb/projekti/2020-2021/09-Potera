import { Component, Input, OnInit } from '@angular/core';
import { ChasePhasesCommonService } from '../services/chase-phases-common.service';

@Component({
  selector: 'app-chase',
  templateUrl: './chase.component.html',
  styleUrls: ['./chase.component.css']
})
export class ChaseComponent implements OnInit {

  title = 'Potera-An';
  //pitanja = new PitanjaComponent();

  //ovo se salje tabli-za-poteru da bi znala tabla do kog reda smo stigli
  receivedAnswerCounter:string = "0";
  receivedAnswerNumber:string;


  receivedNumberOfChassersCorrect:string = "0";


  //Ako je tacan prva faza je u toku
  //playingFirstPhase:boolean = true;

  //Ako je tacan i playingFirstFase netacan druga
  //faza je u toku
  //playingSecondPhase:boolean = false;

  constructor(public CPservice:ChasePhasesCommonService) {
    /*
    setTimeout(() => {
      //console.log(this.pitanja.correctAnswerCounter + "nesto");
    }, 2000);
    */
  }

  public EndOfGame:boolean = this.CPservice.EndOfGame;


  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }

  receiveAnswerCounter(message:string){
    this.receivedAnswerCounter = message;
    console.log(this.receiveAnswerCounter);
  }

  receiveAnswerNumber(message:string){
    this.receivedAnswerNumber = message;
    console.log(this.receivedAnswerNumber + " App")
  }

  receiveNumberOfChassersCorrect(message:string){
    this.receivedNumberOfChassersCorrect = message;
    console.log("Tragacevi odgovori: " + this.receivedNumberOfChassersCorrect);
  }

  public getEndOfGame():boolean{
    return this.CPservice.EndOfGame;
  }

}
