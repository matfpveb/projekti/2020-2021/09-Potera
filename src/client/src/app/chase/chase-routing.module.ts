import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChaseComponent } from './chase.component';

const routes: Routes = [{ path: '', component: ChaseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChaseRoutingModule { }
