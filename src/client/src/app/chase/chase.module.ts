import { TablaZaPoteruComponent } from './tabla-za-poteru/tabla-za-poteru.component';
import { PitanjaComponent } from './pitanja/pitanja.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChaseRoutingModule } from './chase-routing.module';
import { ChaseComponent } from './chase.component';
import { EndPopUpComponent } from './end-pop-up/end-pop-up.component';


@NgModule({
  declarations: [
    ChaseComponent,
    PitanjaComponent,
    TablaZaPoteruComponent,
    EndPopUpComponent
  ],
  imports: [
    CommonModule,
    ChaseRoutingModule
  ],

})
export class ChaseModule { }
