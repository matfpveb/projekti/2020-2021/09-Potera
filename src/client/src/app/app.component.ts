import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CommonModule} from '@angular/common'

declare const $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Potera-An';
  //pitanja = new PitanjaComponent();

  //ovo se salje tabli-za-poteru da bi znala tabla do kog reda smo stigli
  receivedAnswerCounter:string = "0";
  receivedAnswerNumber:string;


  receivedNumberOfChassersCorrect:string = "0";

  //Ako je tacan prva faza je u toku
  playingFirstPhase:boolean = true;

  //Ako je tacan i playingFirstFase netacan druga
  //faza je u toku
  playingSecondPhase:boolean = false;

  constructor() {
    /*
    setTimeout(() => {
      //console.log(this.pitanja.correctAnswerCounter + "nesto");
    }, 2000);
    */
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }

  receiveAnswerCounter(message:string){
    this.receivedAnswerCounter = message;
    console.log(this.receiveAnswerCounter);
  }

  receiveAnswerNumber(message:string){
    this.receivedAnswerNumber = message;
    console.log(this.receivedAnswerNumber + " App")
  }

  receiveNumberOfChassersCorrect(message:string){
    this.receivedNumberOfChassersCorrect = message;
    console.log("Tragacevi odgovori: " + this.receivedNumberOfChassersCorrect);
  }

}
