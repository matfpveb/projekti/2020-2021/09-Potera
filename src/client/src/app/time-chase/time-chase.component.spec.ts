import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeChaseComponent } from './time-chase.component';

describe('TimeChaseComponent', () => {
  let component: TimeChaseComponent;
  let fixture: ComponentFixture<TimeChaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeChaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeChaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
