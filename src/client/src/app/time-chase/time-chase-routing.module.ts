import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimeChaseComponent } from './time-chase.component';

const routes: Routes = [{ path: '', component: TimeChaseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeChaseRoutingModule { }
