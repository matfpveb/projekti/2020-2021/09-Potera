import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeChaseRoutingModule } from './time-chase-routing.module';
import { TimeChaseComponent } from './time-chase.component';
import { PitanjaPrvaFazaComponent } from './pitanja-prva-faza/pitanja-prva-faza.component';


@NgModule({
  declarations: [
    TimeChaseComponent,
    PitanjaPrvaFazaComponent
  ],
  imports: [
    CommonModule,
    TimeChaseRoutingModule
  ]
})
export class TimeChaseModule { }
