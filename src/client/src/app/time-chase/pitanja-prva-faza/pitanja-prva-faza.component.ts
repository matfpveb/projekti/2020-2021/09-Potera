import { ChasePhasesCommonService } from './../../services/chase-phases-common.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {Pitanje} from '../../chase/pitanja/pitanje'
import { environment } from '../../../environments/environment.dev'
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pitanja-prva-faza',
  templateUrl: './pitanja-prva-faza.component.html',
  styleUrls: ['./pitanja-prva-faza.component.css']
})
export class PitanjaPrvaFazaComponent implements OnInit {
  private wrongAnswerBuzzer = new Audio();
  private correctAnswerBuzzer = new Audio();

  //kad se pritisne dugme zapocni igru ovo postaje true
  gameIsRunning:boolean = false;

  // u pitanje.ts je definisana klasa Pitanje
  pitanja: Pitanje [] = [];

  private RandomQuestionNumberArray: number[];

  numberOfQuestion: number = 0;

  id: number;
  questionText: string = "          ";
  answer_1: string = "             ";
  answer_2: string = "             ";
  answer_3: string = "              ";
  correctAnswer: number = 0;

  MoneyEarned:number = 0;
  CORRECT_ANSWER_AWARD:number = 10000;

  lista: any;
  svapitanja: any;
  brpitanja: number;

  getQuestions(){

    let url= environment.PRVAFAZA_BASE_URL+environment.PRVAFAZA.GET_ALL;
    let obs= this.httpClient.get(url);
    obs.subscribe((data: any[]) => {
      this.lista=data;
      this.svapitanja=this.lista.results;
      this.brpitanja=this.svapitanja.length;
      this.svapitanja.forEach(element => {
        this.pitanja.push(new Pitanje(
          element.tekst,
          element.odgovor1,
          element.odgovor2,
          element.odgovor3,
          element.tacanodgovor
        ));
      });
    });

   }

  buttonPressed: boolean = true;


  constructor(private httpClient: HttpClient, private CPservice:ChasePhasesCommonService, private router:Router) {
    this.remainingPathColor = this.COLOR_CODES.info.color;
    this.wrongAnswerBuzzer.src = '../../../assets/test.mp3';
    this.correctAnswerBuzzer.src = '../../../assets/Correct AnswerIdea Sound Effects.mp3';
  }

  ngOnInit(): void {
    this.getQuestions();
    this.wrongAnswerBuzzer.load();
    this.correctAnswerBuzzer.load();
  }



  startGameButton(){
    this.gameIsRunning = true;
    this.generateRandomQuestionNumberArray();

    this.setQuestionAndAnswers();
    this.startTimer();
  }

  public generateRandomQuestionNumberArray(){
    this.RandomQuestionNumberArray = Array.from(Array(this.pitanja.length).keys());
    //pomesa elemente u nizu
    this.RandomQuestionNumberArray.sort(() => Math.random() - 0.5);
  }

  setQuestionAndAnswers() {

    this.questionText = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].text;
    this.answer_1 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_1;
    this.answer_2 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_2;
    this.answer_3 = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].answer_3;
    this.correctAnswer = this.pitanja[this.RandomQuestionNumberArray[this.numberOfQuestion]].correctAnswer;
  }

  onButtonPressed(contestantsAnswer: number) {
    if(contestantsAnswer === this.correctAnswer) {
    this.correctAnswerBuzzer.play();
    this.MoneyEarned += this.CORRECT_ANSWER_AWARD;
    }
    else {
      this.wrongAnswerBuzzer.play();
    }
    this.numberOfQuestion++;
    this.setQuestionAndAnswers();
  }

  /*
    https://css-tricks.com/how-to-create-an-animated-countdown-timer-with-html-css-and-javascript/
    Deo koji se odnosi na tajmer
  */

    //Za animaciju kruga oko tajmera
    COLOR_CODES = {
      info: {
        color: "green"
      }
    };
    remainingPathColor;

    TIME_LIMIT:number = 60;
    timePassed = 0;
    timeLeft:number = 60;

  formatTimeLeft(time) {
    const minutes = Math.floor(time / 60);

    let seconds = time % 60;

    let timeStr = "";

    if(seconds < 10){
      timeStr = '0' + seconds;
    }

    return minutes + ':' + seconds;
  }

  timerInterval = null;

  public checkIfTimeHasPassed(){
    if(this.timeLeft == 1){
      this.buttonPressed = true;
      this.CPservice.setMoneyEarned(this.MoneyEarned);
      clearInterval(this.timerInterval);
      this.router.navigateByUrl('Chase');
    }
  }

  public startTimer() {

    this.timerInterval = setInterval(() => {
      this.checkIfTimeHasPassed();

      this.timePassed += 1;
      this.timeLeft = this.TIME_LIMIT - this.timePassed;
      this.formatTimeLeft(this.timeLeft);
      this.setCircleDasharray();
    }, 1000);
  }

 public calculateTimeFraction(){
    const rawTimeFraction = this.timeLeft / this.TIME_LIMIT;
    return rawTimeFraction - (1 / this.TIME_LIMIT) * (1 - rawTimeFraction);

  }

  //Length = 2pir = 2*pi*45 = 282,6
  FULL_DASH_ARRAY = 283;
  setCircleDasharray() {
    const circleDasharray = `${(
      this.calculateTimeFraction() * this.FULL_DASH_ARRAY
    ).toFixed(0)} 283`;

    document
      .getElementById("base-timer-path-remaining")
      .setAttribute("stroke-dasharray", circleDasharray);
  }

}
