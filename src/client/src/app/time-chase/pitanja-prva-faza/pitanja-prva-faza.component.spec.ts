import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { PitanjaPrvaFazaComponent } from './pitanja-prva-faza.component';
import {} from '@angular/common';

describe('PitanjaPrvaFazaComponent', () => {
  let component: PitanjaPrvaFazaComponent;
  let fixture: ComponentFixture<PitanjaPrvaFazaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PitanjaPrvaFazaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PitanjaPrvaFazaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});