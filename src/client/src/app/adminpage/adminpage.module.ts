import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminpageRoutingModule } from './adminpage-routing.module';
import { AdminpageComponent } from './adminpage.component';
import { Faza1Component } from './faza1/faza1.component';
import { Faza2Component } from './faza2/faza2.component';
import { UsersComponent } from './users/users.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FormsModule }   from '@angular/forms';
import { SuccessComponent } from './success/success.component';
import { FailComponent } from './fail/fail.component';

@NgModule({
  declarations: [
    AdminpageComponent,
    Faza1Component,
    Faza2Component,
    UsersComponent,
    SidebarComponent,
    SuccessComponent,
    FailComponent
  ],
  imports: [
    CommonModule,
    AdminpageRoutingModule,
    FormsModule
  ]
})
export class AdminpageModule { }
