export class Question {
    _id: string;
    tekst: string;
    odgovor1: string;
    odgovor2: string;
    odgovor3: string;
    tacanodgovor: number;
}