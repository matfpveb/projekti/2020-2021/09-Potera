export class User {
    _id: string;
    ime: string;
    prezime: string;
    email: string;
    rodjendan: Date;
    sifra: string;
}