import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.dev';
import { User } from '../models/user';
import { ActivatedRoute } from '@angular/router';
import { FailComponent } from '../fail/fail.component'
import { SuccessComponent } from '../success/success.component'
import $ from 'jquery';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css',
  '../Font-Awesome-4.7.0/css/font-awesome.min.css',
  '../faza2/font.css',
  '../faza2/icon.css',
  '../faza2/bootstrap.min.css']
})
export class UsersComponent implements OnInit {

  constructor(private httpClient: HttpClient, private activatedRoute: ActivatedRoute) { 
    this.tablelist=[];
    this.currentpage=1;
    this.records=5;
  }

  ngOnInit(): void {
    this.allUsers();
    this.trenutniUser= new User();
  }

  listaUsers: any;
  sviUser: any;
  brUsers: number;
  trenutniUser: any;
  currentid: any;
  response: any;
  failcmp= new FailComponent();
  successcmp= new SuccessComponent();
  currentpage: number;
  records: number;
  range = n => [...Array(n).keys()];
  maxpages: any;

  allUsers (){
    
    let url= environment.USERS_BASE_URL+environment.USERS.GET_ALL;
    let obs= this.httpClient.get(url);
    obs.subscribe((data: any[]) => {
      this.listaUsers=data;
      this.sviUser=this.listaUsers.results;
      this.sviUser.forEach(element => {
        element.rodjendan= element.rodjendan.slice(0, 10);
      });
      this.brUsers=this.sviUser.length;
      this.show(this.currentpage);
      this.maxpages= this.range(Math.ceil(this.brUsers/ this.records));
      this.maxpages.reverse();
      });
    
  }
  
  addUser(user){
    let url= environment.USERS_BASE_URL+environment.USERS.CREATE_USER;
    let usr= new User();
    usr.ime= user.value.ime;
    usr.prezime= user.value.prezime;
    usr.email= user.value.email;
    usr.sifra= user.value.sifra;
    usr.rodjendan= user.value.rodjendan;
    let obs= this.httpClient.post(url,usr);
    
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        this.successcmp.toggle();
        this.allUsers();
      }
      });
  }


  deleteUser(){
    let url= environment.USERS_BASE_URL+ environment.USERS.DELETE_USER
    +this.currentid;

    let obs= this.httpClient.delete(url);
    
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        this.successcmp.toggle();
        this.allUsers();
      }
     });

  }

  updateUser(user){
    let usr=this.trenutniUser;
    if(user.value.editime !="") usr.ime= user.value.editime;
    if(user.value.editprezime !="") usr.prezime= user.value.editprezime;
    if(user.value.editemail !="") usr.email= user.value.editemail;
    if(user.value.editrodjendan !="") usr.rodjendan= user.value.editrodjendan;
    usr.sifra=user.value.sifra;
    let url= environment.USERS_BASE_URL+ environment.USERS.UPDATE_USER+
    this.currentid;
    
    let obs= this.httpClient.put(url,usr);
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        $('#addQuestionModal').hide();
        $(".modal-backdrop.in").hide();
        this.successcmp.toggle();
        this.allUsers();
      }
     });
     
  }

  search(id){
    this.currentid=id;
    let url= environment.USERS_BASE_URL+ environment.USERS.SEARCH_BY_ID
    +this.currentid;
    let obs= this.httpClient.get(url);
    
    let placeholder;
    obs.subscribe((data: any[]) => {
      placeholder=data;
      this.trenutniUser=placeholder.userResponse;
      this.trenutniUser.rodjendan= this.trenutniUser.rodjendan.slice(0, 10);
      console.log(this.trenutniUser);
      });
  }

  tablelist: any;

  show(pageNum)
  {
    let i=0;
    this.tablelist.length=0;
    if((pageNum-1)<= (this.brUsers/this.records)){
     while(i<this.records && i< (this.brUsers - this.records*(pageNum-1))){
       let index=(pageNum-1)*this.records +i;
       this.tablelist.push(this.sviUser[index]);
        i++;
      }
      this.currentpage=pageNum;
    }
  }

  possible= [5,1,2,10];

  onChange(value){
    this.records=value;
    this.currentpage=1;
    this.allUsers();
  }

}
