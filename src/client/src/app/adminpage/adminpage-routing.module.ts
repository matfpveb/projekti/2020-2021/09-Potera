import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminpageComponent } from './adminpage.component';
import { FailComponent } from './fail/fail.component';
import { Faza1Component } from './faza1/faza1.component';
import { Faza2Component } from './faza2/faza2.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [{ path: '', component: AdminpageComponent },
{ path: 'faza1', component: Faza1Component },
{ path: 'faza2', component: Faza2Component },
{ path: 'users', component: UsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminpageRoutingModule { }
