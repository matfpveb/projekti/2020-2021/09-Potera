import { Component, OnInit } from '@angular/core';
import $ from 'jquery';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css',
  '../Font-Awesome-4.7.0/css/font-awesome.min.css',
  '../faza2/font.css',
  '../faza2/icon.css',
  '../faza2/bootstrap.min.css']
})
export class SuccessComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  toggle(): void{
    console.log("OPENED MODAL");
    $('#successModal').toggle();
  }
}
