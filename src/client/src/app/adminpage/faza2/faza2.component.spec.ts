import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Faza2Component } from './faza2.component';

describe('Faza2Component', () => {
  let component: Faza2Component;
  let fixture: ComponentFixture<Faza2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Faza2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Faza2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
