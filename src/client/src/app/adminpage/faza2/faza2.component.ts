import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.dev';
import { Question } from '../models/question';
import { ActivatedRoute } from '@angular/router';
import { FailComponent } from '../fail/fail.component'
import { SuccessComponent } from '../success/success.component'
import $ from 'jquery';

@Component({
  selector: 'app-faza2',
  templateUrl: './faza2.component.html',
  styleUrls: ['./faza2.component.css',
              '../Font-Awesome-4.7.0/css/font-awesome.min.css',
              './font.css',
              './icon.css',
              './bootstrap.min.css'
            ]
})
export class Faza2Component implements OnInit {

  constructor(private httpClient: HttpClient, private activatedRoute: ActivatedRoute) { 
    this.tablelist=[];
    this.currentpage=1;
    this.records=5;
  }

  ngOnInit(): void {
    this.glavnaPitanja();
    this.trenutnopitanje= new Question();
  }

  currentid: any;
  lista: any;
  svapitanja: any;
  brpitanja: number;
  trenutnopitanje: any;
  response: any;
  failcmp= new FailComponent();
  successcmp= new SuccessComponent();
  currentpage: number;
  records: number;
  range = n => [...Array(n).keys()];
  maxpages: any;

  glavnaPitanja (){
    
    let url= environment.QUESTIONS_BASE_URL+environment.QUESTIONS.GET_ALL;
    let obs= this.httpClient.get(url);
    obs.subscribe((data: any[]) => {
      this.lista=data;
      this.svapitanja=this.lista.results;
      this.brpitanja=this.svapitanja.length;
      this.show(this.currentpage);
      this.maxpages= this.range(Math.ceil(this.brpitanja/ this.records));
      this.maxpages.reverse();
      });
    
  }

  addPitanje(pitanje){
    let url= environment.QUESTIONS_BASE_URL+environment.QUESTIONS.CREATE_QUESTION;
    let qst= new Question();
    qst.tekst= pitanje.value.tekst;
    qst.odgovor1= pitanje.value.odgovor1;
    qst.odgovor2= pitanje.value.odgovor2;
    qst.odgovor3= pitanje.value.odgovor3;
    qst.tacanodgovor= pitanje.value.tacanodgovor;
    let obs= this.httpClient.post(url,qst);
    
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        this.successcmp.toggle();
        this.glavnaPitanja();
      }
      });
  }


  deletePitanje(){
    let url= environment.QUESTIONS_BASE_URL+ environment.QUESTIONS.DELETE_QUESTION
    +this.currentid;

    let obs= this.httpClient.delete(url);
    
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        this.successcmp.toggle();
        this.glavnaPitanja();
      }
     });

  }

  updatePitanje(pitanje){
    let qst=this.trenutnopitanje;
    if(pitanje.value.edittekst !="") qst.tekst= pitanje.value.edittekst;
    if(pitanje.value.editodgovor1 !="") qst.odgovor1= pitanje.value.editodgovor1;
    if(pitanje.value.editodgovor2 !="") qst.odgovor2= pitanje.value.editodgovor2;
    if(pitanje.value.editodgovor3 !="") qst.odgovor3= pitanje.value.editodgovor3;
    if(pitanje.value.edittacanodgovor !="") qst.tacanodgovor= pitanje.value.edittacanodgovor;
    
    let url= environment.QUESTIONS_BASE_URL+ environment.QUESTIONS.UPDATE_QUESTION+
    this.currentid;
    
    let obs= this.httpClient.put(url,qst);
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        $('#addQuestionModal').hide();
        $(".modal-backdrop.in").hide();
        this.successcmp.toggle();
        this.glavnaPitanja();
      }
     });

  }

  search(id){
    this.currentid=id;
    let url= environment.QUESTIONS_BASE_URL+ environment.QUESTIONS.SEARCH_BY_ID
    +this.currentid;
    let obs= this.httpClient.get(url);
    
    let placeholder;
    obs.subscribe((data: any[]) => {
      placeholder=data;
      this.trenutnopitanje=placeholder.questionResponse;
      console.log(this.trenutnopitanje);
      });
  }

  tablelist: any;

  show(pageNum)
  {
    let i=0;
    this.tablelist.length=0;
    if((pageNum-1)<= (this.brpitanja/this.records)){
     while(i<this.records && i<(this.brpitanja - this.records*(pageNum-1))){
       let index=(pageNum-1)*this.records +i;
       this.tablelist.push(this.svapitanja[index]);
        i++;
      }
      this.currentpage=pageNum;
    }
  }

  possible= [5,1,2,10];

  onChange(value){
    this.records=value;
    this.currentpage=1;
    this.glavnaPitanja();
  }

}
