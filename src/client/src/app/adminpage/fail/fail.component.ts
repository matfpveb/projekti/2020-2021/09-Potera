import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import $ from 'jquery';

@Component({
  selector: 'app-fail',
  templateUrl: './fail.component.html',
  styleUrls: ['./fail.component.css',
  '../Font-Awesome-4.7.0/css/font-awesome.min.css',
  '../faza2/font.css',
  '../faza2/icon.css',
  '../faza2/bootstrap.min.css']
})
export class FailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

 toggle(): void{
    console.log("OPENED MODAL");
    $('#failModal').toggle();
  }

}

