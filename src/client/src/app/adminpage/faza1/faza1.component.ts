import { Component, ElementRef, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.dev';
import { Question } from '../models/question';
import { ActivatedRoute } from '@angular/router';
import { FailComponent } from '../fail/fail.component'
import { SuccessComponent } from '../success/success.component'
import $ from 'jquery';

@Component({
  selector: 'app-faza1',
  templateUrl: './faza1.component.html',
  styleUrls: ['./faza1.component.css',
  '../Font-Awesome-4.7.0/css/font-awesome.min.css',
  '../faza2/font.css',
  '../faza2/icon.css',
  '../faza2/bootstrap.min.css']
})
export class Faza1Component implements OnInit {

  constructor(private httpClient: HttpClient, private activatedRoute: ActivatedRoute) { 
    this.tablelist=[];
    this.currentpage=1;
    this.records=5;
  }

  ngOnInit(): void {
    this.prvafazaPitanja();
    this.trenutnopitanjefaza= new Question();

  }

  listafaza: any;
  svapitanjafaza: any;
  brpitanjafaza: number;
  trenutnopitanjefaza: any;
  currentid: any;
  response: any;
  failcmp= new FailComponent();
  successcmp= new SuccessComponent();
  currentpage: number;
  records: number;
  range = n => [...Array(n).keys()];
  maxpages: any;

  prvafazaPitanja (){
    
    let url= environment.PRVAFAZA_BASE_URL+environment.QUESTIONS.GET_ALL;
    let obs= this.httpClient.get(url);
    obs.subscribe((data: any[]) => {
      this.listafaza=data;
      this.svapitanjafaza=this.listafaza.results;
      this.brpitanjafaza=this.svapitanjafaza.length;
      this.show(this.currentpage);
      this.maxpages= this.range(Math.ceil(this.brpitanjafaza/ this.records));
      this.maxpages.reverse();
      console.log(this.maxpages);
      });
    
  }
  
  addPitanje(pitanje){
    let url= environment.PRVAFAZA_BASE_URL+environment.QUESTIONS.CREATE_QUESTION;
    let qst= new Question();

    qst.tekst= pitanje.value.tekst;
    qst.odgovor1= pitanje.value.odgovor1;
    qst.odgovor2= pitanje.value.odgovor2;
    qst.odgovor3= pitanje.value.odgovor3;
    qst.tacanodgovor= pitanje.value.tacanodgovor;
    let obs= this.httpClient.post(url,qst);
    
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        this.successcmp.toggle();
        this.prvafazaPitanja();
      }
      });
  }


  deletePitanje(){
    let url= environment.PRVAFAZA_BASE_URL+ environment.QUESTIONS.DELETE_QUESTION
    +this.currentid;

    let obs= this.httpClient.delete(url);
    
    obs.subscribe((data: any[]) => {
       console.log(data);
       this.response=data;
       if(this.response.status!=200){
         this.failcmp.toggle();
       }
 
       else{
         this.successcmp.toggle();
         this.prvafazaPitanja();
       }
      });

  }

  updatePitanje(pitanje){
    let qst=this.trenutnopitanjefaza;
    if(pitanje.value.edittekst !="") qst.tekst= pitanje.value.edittekst;
    if(pitanje.value.editodgovor1 !="") qst.odgovor1= pitanje.value.editodgovor1;
    if(pitanje.value.editodgovor2 !="") qst.odgovor2= pitanje.value.editodgovor2;
    if(pitanje.value.editodgovor3 !="") qst.odgovor3= pitanje.value.editodgovor3;
    if(pitanje.value.edittacanodgovor !="") qst.tacanodgovor= pitanje.value.edittacanodgovor;
    
    let url= environment.PRVAFAZA_BASE_URL+ environment.QUESTIONS.UPDATE_QUESTION+
    this.currentid;
    
    let obs= this.httpClient.put(url,qst);
    obs.subscribe((data: any[]) => {
      console.log(data);
      this.response=data;
      if(this.response.status!=200){
        this.failcmp.toggle();
      }

      else{
        $('#addQuestionModal').hide();
        $(".modal-backdrop.in").hide();
        this.successcmp.toggle();
        this.prvafazaPitanja();
      }
     });

  }

  search(id){
    this.currentid=id;
    let url= environment.PRVAFAZA_BASE_URL+ environment.QUESTIONS.SEARCH_BY_ID
    +this.currentid;
    let obs= this.httpClient.get(url);
    
    let placeholder;
    obs.subscribe((data: any[]) => {
      placeholder=data;
      this.trenutnopitanjefaza=placeholder.questionResponse;
      console.log(this.trenutnopitanjefaza);
      });
  }

  tablelist: any;

  show(pageNum)
  {
    let i=0;
    this.tablelist.length=0;
    if((pageNum-1)<= (this.brpitanjafaza/this.records)){
     while(i<this.records && i< (this.brpitanjafaza - this.records*(pageNum-1))){
       let index=(pageNum-1)*this.records +i;
       this.tablelist.push(this.svapitanjafaza[index]);
        i++;
      }
      this.currentpage=pageNum;
    }
  }

  possible= [5,1,2,10];

  onChange(value){
    this.records=value;
    this.currentpage=1;
    this.prvafazaPitanja();
  }

}
