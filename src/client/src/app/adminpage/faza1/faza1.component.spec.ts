import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Faza1Component } from './faza1.component';

describe('Faza1Component', () => {
  let component: Faza1Component;
  let fixture: ComponentFixture<Faza1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Faza1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Faza1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
