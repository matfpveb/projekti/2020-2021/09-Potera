import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChasePhasesCommonService {

  private MoneyEarned:number;

  private EndMessage:string;

  public ShowEndPopUp:boolean;

  public EndOfGame:boolean;

  constructor() {
    this.EndOfGame = false;
   }

  public getMoneyEarned(): number{
    return this.MoneyEarned;
  }

  public setMoneyEarned(money:number){
    this.MoneyEarned = money;
  }

  public getEndMessage(): string {
    return this.EndMessage;
  }

  public setEndMessage(msg:string){
    this.EndMessage = msg;
  }

  public setEndOfGame(){
    this.EndOfGame = true;
    console.log('Ovde postaje true!');
  }


}
