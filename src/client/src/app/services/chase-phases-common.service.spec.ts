import { TestBed } from '@angular/core/testing';

import { ChasePhasesCommonService } from './chase-phases-common.service';

describe('ChasePhasesCommonService', () => {
  let service: ChasePhasesCommonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChasePhasesCommonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
