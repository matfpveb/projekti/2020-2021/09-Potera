export const environment = {
    production: false,
    BASE_URL: 'http://localhost:3000',
    QUESTIONS_BASE_URL: 'http://localhost:3000/questions/',
    QUESTIONS: {
        GET_ALL: 'list',
        CREATE_QUESTION: 'add',
        UPDATE_QUESTION: 'update?questionId=',
        DELETE_QUESTION: 'delete?questionId=',
        SEARCH_QUESTION: 'search',
        SEARCH_BY_ID: 'id?questionId='
    },
    PRVAFAZA_BASE_URL: 'http://localhost:3000/prvafaza/',
    PRVAFAZA: {
        GET_ALL: 'list',
        CREATE_QUESTION: 'add',
        UPDATE_QUESTION: 'update?questionId=',
        DELETE_QUESTION: 'delete?questionId=',
        SEARCH_QUESTION: 'search',
        SEARCH_BY_ID: 'id?questionId='
    },
    USERS_BASE_URL: 'http://localhost:3000/users/',
    USERS: {
        GET_ALL: 'list',
        CREATE_USER: 'add',
        UPDATE_USER: 'update?userId=',
        DELETE_USER: 'delete?userId=',
        SEARCH_USER: 'search',
        SEARCH_BY_ID: 'id?userId=',
        LOGIN: 'login'
    }
  };
